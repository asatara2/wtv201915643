var mysql = require('mysql');
var express = require('express');
var path = require('path');
const bodyParser = require('body-parser');
var app=express();

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(express.static(path.join(__dirname ,'public')));

let con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "password",
    database: "vjezba8"
  });

app.get('/imenik', (req,res) => {

    con.connect((err) => {
        if (err) throw err;

        con.query("SELECT * FROM tabela", function (err, result) {
            if (err) throw err;

            let tabela = ' <table style="width:100%"><tr><th>Ime i prezime</th><th>Adresa</th><th>Broj telefona</th></tr>';
            result.forEach(element => {
                console.log(element.ime);
                console.log("----");
                tabela += '<tr><td>'+ element.ime + '</td><td>' + element.adresa + '</td><td>' + element.broj_telefona + '</td></tr>';
            });

            tabela += '</table>';
            res.send(tabela);
          });
    })


})

app.get('/unesi', (req,res) => {
    res.sendFile(path.join(__dirname, 'public', 'forma.html'));
})

app.post('/unos', (req,res) => {
    let tijelo = req.body;
    console.log( tijelo );

    con.connect(function(err) {
        if (err) throw err;
        console.log(req.body);
        console.log("Connected!");
        var sql = "INSERT INTO tabela (ime, adresa, broj_telefona) VALUES ('"+ req.body.ime +"', '"+ req.body.adresa +"', '"+ req.body.broj +"')";
        con.query(sql, function (err, result) {
          if (err) throw err;
          console.log("Tabela ažurirana!");
          res.send("Success!")
        });
    });
});


var server = app.listen(8000, ()=>{
    console.log('Server started!');
})